<div align="center">![Brick Breaker Evolution](/Screenshots/main.png)</div>

<br />

Brick Breaker Evolution revolutionises the classic breakout genre with a <b>unique twist</b>: Experience unrestricted paddle movement in all directions, while the ball is affected by <b>gravity, friction, torque, and paddle impact</b> driven by a powerful <b>2D physics engine</b>.

This is the <b>most dynamic brick breaker game</b> you have ever tried. Conquer the <b>50 increasingly challenging levels</b> and unlock a range of items that can aid or hinder your progress. Strategise and make split-second decisions to utilise power-ups effectively and overcome obstacles.

Complete <b>various challenges</b> to earn Brick Coins, valuable items, and extra balls, and <b>replay any level as many times as you like</b>, as each game promises a unique experience.

With its meticulously designed levels and <b>fast-paced, addictive gameplay</b>, Brick Breaker Evolution seamlessly blends nostalgia with a modern twist on retro brick breaker mechanics.

<b>Get ready to break some bricks!</b>

<br />

# Features

✅ Powered by a real 2D physics engine

✅ Hours of highly addictive, fast-paced, dynamic gameplay

✅ 50 increasingly difficult levels (more to come)

✅ 16 unique items to unlock as you advance in the game (more to come)

✅ Retro-inspired hand-drawn graphics with a minimalist, distraction-free interface

✅ Engaging challenges with rewarding prizes

✅ Five difficulty levels

✅ Fully offline play, no accounts or logins required (internet connection only needed for advertisements, and in-app purchases)

✅ No disruptive ads (and you can disable the few that appear without having to spend money)

✅ Compact download size (~30MB)

<div align="center">
<figure class="video_container">
  <iframe src="/Videos/BrickPong__PlayStore_Final.mp4" frameborder="0" allowfullscreen="true"> 
</iframe>
</figure>
</div>




<div align="center"><a href='https://play.google.com/store/apps/details?id=com.attilaoroszdev.brickpong&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a></div>

<div align="center"><sup>Google Play and the Google Play logo are trademarks of Google LLC.</sup></div>
